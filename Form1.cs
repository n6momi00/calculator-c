﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ActiveControl = resultLabel;
        }

        private void Number_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            Label lbl = resultLabel;
            
            if (lbl.Text == "0")
            {
                lbl.Text = btn.Text;
            }
            else if (lbl.Text.EndsWith(" "))
            {
                if (btn.Text != "0")
                {
                    lbl.Text += btn.Text;
                }
            }
            else
            {
                lbl.Text += btn.Text;
            }

            this.ActiveControl = resultLabel;
        }

        private void Operator_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            Label lbl = resultLabel;

            if (!lbl.Text.EndsWith(" "))
            {
                lbl.Text += " " + btn.Text + " ";
            }

            this.ActiveControl = resultLabel;
        }

        private void DeleteChar(object sender, EventArgs e)
        {
            Label lbl = resultLabel;

            if (lbl.Text.Length > 1)
            {
                lbl.Text = lbl.Text.Remove(lbl.Text.Length - (lbl.Text.EndsWith(" ") ? 3 : 1 ));
            }
            else
            {
                lbl.Text = "0";
            }

            this.ActiveControl = resultLabel;
        }

        private void ClearEntry(object sender, EventArgs e)
        {
            Label lbl = resultLabel;

            while (!lbl.Text.EndsWith(" ") && lbl.Text.Length > 1)
            {
                DeleteChar(sender, e);
            }

            if (lbl.Text.Length == 1)
            {
                lbl.Text = "0";
            }

            this.ActiveControl = resultLabel;
        }

        private void Clear(object sender, EventArgs e)
        {
            Label lbl = resultLabel;
            lbl.Text = "0";
            this.ActiveControl = resultLabel;
        }

        private void Calculate(object sender, EventArgs e)
        {
            Label lbl = resultLabel;
            Double result = Eval(lbl.Text);
            lbl.Text = result.ToString();
            this.ActiveControl = resultLabel;
        }

        private static double Eval(string expression)
        {
            DataTable table = new DataTable();
            return Convert.ToDouble(table.Compute(expression, String.Empty));
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Back:
                    DeleteChar(sender, e);
                    break;
                case Keys.Enter:
                    Calculate(sender, e);
                    break;
                case Keys.D0:
                    break;
                case Keys.D1:
                    break;
                case Keys.D2:
                    break;
                case Keys.D3:
                    break;
                case Keys.D4:
                    break;
                case Keys.D5:
                    break;
                case Keys.D6:
                    break;
                case Keys.D7:
                    break;
                case Keys.D8:
                    break;
                case Keys.D9:
                    break;
                case Keys.NumPad0:
                    break;
                case Keys.NumPad1:
                    break;
                case Keys.NumPad2:
                    break;
                case Keys.NumPad3:
                    break;
                case Keys.NumPad4:
                    break;
                case Keys.NumPad5:
                    break;
                case Keys.NumPad6:
                    break;
                case Keys.NumPad7:
                    break;
                case Keys.NumPad8:
                    break;
                case Keys.NumPad9:
                    break;
                case Keys.Multiply:
                    break;
                case Keys.Add:
                    break;
                case Keys.Subtract:
                    break;
                case Keys.Divide:
                    break;
                default:
                    break;
            }
        }
    }
}
