﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_multiply = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_minus = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_plus = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_equal = new System.Windows.Forms.Button();
            this.btn_comma = new System.Windows.Forms.Button();
            this.btn_0 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            this.btn_division = new System.Windows.Forms.Button();
            this.btn_del = new System.Windows.Forms.Button();
            this.btn_c = new System.Windows.Forms.Button();
            this.btn_ce = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_7
            // 
            this.btn_7.Location = new System.Drawing.Point(12, 116);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(67, 50);
            this.btn_7.TabIndex = 0;
            this.btn_7.TabStop = false;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = true;
            this.btn_7.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_8
            // 
            this.btn_8.Location = new System.Drawing.Point(85, 116);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(67, 50);
            this.btn_8.TabIndex = 1;
            this.btn_8.TabStop = false;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = true;
            this.btn_8.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_multiply
            // 
            this.btn_multiply.Location = new System.Drawing.Point(231, 116);
            this.btn_multiply.Name = "btn_multiply";
            this.btn_multiply.Size = new System.Drawing.Size(67, 50);
            this.btn_multiply.TabIndex = 3;
            this.btn_multiply.TabStop = false;
            this.btn_multiply.Text = "*";
            this.btn_multiply.UseVisualStyleBackColor = true;
            this.btn_multiply.Click += new System.EventHandler(this.Operator_Click);
            // 
            // btn_9
            // 
            this.btn_9.Location = new System.Drawing.Point(158, 116);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(67, 50);
            this.btn_9.TabIndex = 2;
            this.btn_9.TabStop = false;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = true;
            this.btn_9.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_minus
            // 
            this.btn_minus.Location = new System.Drawing.Point(231, 172);
            this.btn_minus.Name = "btn_minus";
            this.btn_minus.Size = new System.Drawing.Size(67, 50);
            this.btn_minus.TabIndex = 7;
            this.btn_minus.TabStop = false;
            this.btn_minus.Text = "-";
            this.btn_minus.UseVisualStyleBackColor = true;
            this.btn_minus.Click += new System.EventHandler(this.Operator_Click);
            // 
            // btn_6
            // 
            this.btn_6.Location = new System.Drawing.Point(158, 172);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(67, 50);
            this.btn_6.TabIndex = 6;
            this.btn_6.TabStop = false;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = true;
            this.btn_6.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_5
            // 
            this.btn_5.Location = new System.Drawing.Point(85, 172);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(67, 50);
            this.btn_5.TabIndex = 5;
            this.btn_5.TabStop = false;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = true;
            this.btn_5.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_4
            // 
            this.btn_4.Location = new System.Drawing.Point(12, 172);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(67, 50);
            this.btn_4.TabIndex = 4;
            this.btn_4.TabStop = false;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = true;
            this.btn_4.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_plus
            // 
            this.btn_plus.Location = new System.Drawing.Point(231, 228);
            this.btn_plus.Name = "btn_plus";
            this.btn_plus.Size = new System.Drawing.Size(67, 50);
            this.btn_plus.TabIndex = 11;
            this.btn_plus.TabStop = false;
            this.btn_plus.Text = "+";
            this.btn_plus.UseVisualStyleBackColor = true;
            this.btn_plus.Click += new System.EventHandler(this.Operator_Click);
            // 
            // btn_3
            // 
            this.btn_3.Location = new System.Drawing.Point(158, 228);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(67, 50);
            this.btn_3.TabIndex = 10;
            this.btn_3.TabStop = false;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = true;
            this.btn_3.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_2
            // 
            this.btn_2.Location = new System.Drawing.Point(85, 228);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(67, 50);
            this.btn_2.TabIndex = 9;
            this.btn_2.TabStop = false;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = true;
            this.btn_2.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_1
            // 
            this.btn_1.Location = new System.Drawing.Point(12, 228);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(67, 50);
            this.btn_1.TabIndex = 8;
            this.btn_1.TabStop = false;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = true;
            this.btn_1.Click += new System.EventHandler(this.Number_Click);
            // 
            // btn_equal
            // 
            this.btn_equal.Location = new System.Drawing.Point(231, 284);
            this.btn_equal.Name = "btn_equal";
            this.btn_equal.Size = new System.Drawing.Size(67, 50);
            this.btn_equal.TabIndex = 15;
            this.btn_equal.TabStop = false;
            this.btn_equal.Text = "=";
            this.btn_equal.UseVisualStyleBackColor = true;
            this.btn_equal.Click += new System.EventHandler(this.Calculate);
            // 
            // btn_comma
            // 
            this.btn_comma.Location = new System.Drawing.Point(158, 284);
            this.btn_comma.Name = "btn_comma";
            this.btn_comma.Size = new System.Drawing.Size(67, 50);
            this.btn_comma.TabIndex = 14;
            this.btn_comma.TabStop = false;
            this.btn_comma.Text = ",";
            this.btn_comma.UseVisualStyleBackColor = true;
            // 
            // btn_0
            // 
            this.btn_0.Location = new System.Drawing.Point(85, 284);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(67, 50);
            this.btn_0.TabIndex = 13;
            this.btn_0.TabStop = false;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = true;
            this.btn_0.Click += new System.EventHandler(this.Number_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(12, 284);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(67, 50);
            this.button16.TabIndex = 12;
            this.button16.TabStop = false;
            this.button16.Text = "+/-";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // resultLabel
            // 
            this.resultLabel.Location = new System.Drawing.Point(12, 20);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(289, 37);
            this.resultLabel.TabIndex = 16;
            this.resultLabel.Text = "0";
            this.resultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn_division
            // 
            this.btn_division.Location = new System.Drawing.Point(231, 60);
            this.btn_division.Name = "btn_division";
            this.btn_division.Size = new System.Drawing.Size(67, 50);
            this.btn_division.TabIndex = 20;
            this.btn_division.TabStop = false;
            this.btn_division.Text = "/";
            this.btn_division.UseVisualStyleBackColor = true;
            this.btn_division.Click += new System.EventHandler(this.Operator_Click);
            // 
            // btn_del
            // 
            this.btn_del.Location = new System.Drawing.Point(158, 60);
            this.btn_del.Name = "btn_del";
            this.btn_del.Size = new System.Drawing.Size(67, 50);
            this.btn_del.TabIndex = 19;
            this.btn_del.TabStop = false;
            this.btn_del.Text = "DEL";
            this.btn_del.UseVisualStyleBackColor = true;
            this.btn_del.Click += new System.EventHandler(this.DeleteChar);
            // 
            // btn_c
            // 
            this.btn_c.Location = new System.Drawing.Point(85, 60);
            this.btn_c.Name = "btn_c";
            this.btn_c.Size = new System.Drawing.Size(67, 50);
            this.btn_c.TabIndex = 18;
            this.btn_c.TabStop = false;
            this.btn_c.Text = "C";
            this.btn_c.UseVisualStyleBackColor = true;
            this.btn_c.Click += new System.EventHandler(this.Clear);
            // 
            // btn_ce
            // 
            this.btn_ce.Location = new System.Drawing.Point(12, 60);
            this.btn_ce.Name = "btn_ce";
            this.btn_ce.Size = new System.Drawing.Size(67, 50);
            this.btn_ce.TabIndex = 17;
            this.btn_ce.TabStop = false;
            this.btn_ce.Text = "CE";
            this.btn_ce.UseVisualStyleBackColor = true;
            this.btn_ce.Click += new System.EventHandler(this.ClearEntry);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 351);
            this.Controls.Add(this.btn_division);
            this.Controls.Add(this.btn_del);
            this.Controls.Add(this.btn_c);
            this.Controls.Add(this.btn_ce);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.btn_equal);
            this.Controls.Add(this.btn_comma);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.btn_plus);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.btn_minus);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_multiply);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_7);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_multiply;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_minus;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_plus;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_equal;
        private System.Windows.Forms.Button btn_comma;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button btn_division;
        private System.Windows.Forms.Button btn_del;
        private System.Windows.Forms.Button btn_c;
        private System.Windows.Forms.Button btn_ce;
    }
}

